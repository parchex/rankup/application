<?php declare(strict_types=1);

namespace Fixtures\RankUp\Catalog;

use Parchex\Lump\Fixtures\Charger\Value;
use Parchex\Lump\Fixtures\ObjectBuilder;
use RankUp\Catalog\Domain\Candidate;
use RankUp\Catalog\Domain\CandidateId;
use RankUp\Catalog\Domain\CardId;

/**
 * @method \RankUp\Catalog\Domain\Candidate build()
 */
class CandidateBuilder extends ObjectBuilder
{
    public static function create(): self
    {
        return static::builder(
            Candidate::class,
            [CardProvider::class, CandidateProvider::class]
        );
    }

    public function withCandidateId($candidateId): self
    {
        return $this->with(
            'candidateId',
            Value::set($candidateId)->identifier(CandidateId::class)
        );
    }

    public function withTitle($title): self
    {
        return $this->with(
            'title',
            Value::set((string) $title)
        );
    }

    public function withPresentation($presentation): self
    {
        return $this->with(
            'presentation',
            Value::set((string) $presentation)
        );
    }

    public function withCardId($cardId): self
    {
        return $this->with(
            'cardId',
            Value::set($cardId)->identifier(CardId::class)
        );
    }
}
