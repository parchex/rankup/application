<?php declare(strict_types=1);

namespace Fixtures\RankUp\Catalog;

use Parchex\Lump\Fixtures\Faker;
use RankUp\Catalog\Domain\Candidate;
use RankUp\Catalog\Domain\CandidateId;

class CandidateProvider extends Faker
{
    public static function candidate(): Candidate
    {
        return static::entity(CandidateBuilder::class);
    }

    public function candidateId(): CandidateId
    {
        return CandidateId::fromString($this->generator->uuid);
    }

    public function title(): string
    {
        return $this->generator->words($this->generator->numberBetween(4, 9), true);
    }

    public function presentation(): string
    {
        return $this->generator->sentence(32);
    }

    /**
     * {@inheritdoc}
     *
     * @return array<Candidate>
     */
    public static function candidates(int $total): array
    {
        return static::entitiesTotal(CandidateBuilder::class, $total);
    }
}
