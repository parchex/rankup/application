<?php declare(strict_types=1);

namespace Fixtures\RankUp\Catalog;

use Parchex\Lump\Fixtures\Faker;
use RankUp\Catalog\Domain\Card;
use RankUp\Catalog\Domain\CardId;

class CardProvider extends Faker
{
    public static function card(): Card
    {
        return static::entity(CardBuilder::class);
    }

    public function cardId(): CardId
    {
        return CardId::fromString($this->generator->uuid);
    }

    public function title(): string
    {
        return $this->generator->words(5, true);
    }

    public function summary(): string
    {
        return $this->generator->text(50);
    }

    public function story(): string
    {
        return $this->generator->text();
    }
}
