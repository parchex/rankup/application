<?php declare(strict_types=1);

namespace Fixtures\RankUp\Catalog;

use Parchex\Lump\Fixtures\Charger\Value;
use Parchex\Lump\Fixtures\ObjectBuilder;
use RankUp\Catalog\Domain\Candidate;
use RankUp\Catalog\Domain\Ranking;
use RankUp\Catalog\Domain\RankingId;

/**
 * @method \RankUp\Catalog\Domain\Ranking build()
 */
class RankingBuilder extends ObjectBuilder
{
    public static function create(): self
    {
        return static::builder(
            Ranking::class,
            [RankingProvider::class]
        );
    }

    public function withRankingId($rankingId): self
    {
        return $this->with(
            'rankingId',
            Value::set($rankingId)->identifier(RankingId::class)
        );
    }

    public function withTitle($title): self
    {
        return $this->with(
            'title',
            Value::set((string) $title)
        );
    }

    public function withDescription($description): self
    {
        return $this->with(
            'description',
            Value::set((string) $description)
        );
    }

    public function withMaximumCandidates($maximumCandidates): self
    {
        return $this->with(
            'maximumCandidates',
            Value::set((int) $maximumCandidates)
        );
    }

    public function withCandidates($candidates): self
    {
        $counter = 0;

        $this->with(
            'candidates',
            Value::set($candidates)
                ->collection()
                ->instance(
                    Candidate::class,
                    static function ($value) use (&$counter) {
                        $counter++;

                        return CandidateBuilder::create()
                            ->withCandidateId($value)
                            ->build();
                    }
                )
        );

        if ($this->filler->valueOf('maximumCandidates')->get() < $counter) {
            $this->withMaximumCandidates($counter);
        }

        return $this;
    }
}
