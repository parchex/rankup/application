<?php declare(strict_types=1);

namespace Fixtures\RankUp\Catalog;

use Parchex\Lump\Fixtures\Faker;
use RankUp\Catalog\Domain\Candidate;
use RankUp\Catalog\Domain\Ranking;
use RankUp\Catalog\Domain\RankingId;

class RankingProvider extends Faker
{
    public static function ranking(): Ranking
    {
        return static::entity(RankingBuilder::class);
    }

    public function rankingId(): RankingId
    {
        return RankingId::fromString($this->generator->uuid);
    }

    public function title(): string
    {
        return $this->generator->words(7, true);
    }

    public function description(): string
    {
        return $this->generator->text(100);
    }

    public function maximumCandidates(): int
    {
        return Ranking::MAXIMUM_CANDIDATES_DEFAULT;
    }

    /**
     * {@inheritdoc}
     *
     * @return array<Candidate>
     */
    public function candidates(int $min = 2, int $max = Ranking::MAXIMUM_CANDIDATES_DEFAULT): array
    {
        return static::entitiesBetween(CandidateBuilder::class, max(1, $min), max($min, $max));
    }
}
