<?php declare(strict_types=1);

namespace Fixtures\RankUp\Catalog;

use Parchex\Lump\Fixtures\Charger\Value;
use Parchex\Lump\Fixtures\ObjectBuilder;
use RankUp\Catalog\Domain\Card;
use RankUp\Catalog\Domain\CardId;

/**
 * @method \RankUp\Catalog\Domain\Card build()
 */
class CardBuilder extends ObjectBuilder
{
    public static function create(): self
    {
        return static::builder(
            Card::class,
            [CardProvider::class]
        );
    }

    public function withCardId($cardId): self
    {
        return $this->with(
            'cardId',
            Value::set($cardId)->identifier(CardId::class)
        );
    }

    public function withTitle($title): self
    {
        return $this->with(
            'title',
            Value::set((string) $title)
        );
    }

    public function withSummary($summary): self
    {
        return $this->with(
            'summary',
            Value::set((string) $summary)
        );
    }

    public function withStory($story): self
    {
        return $this->with(
            'story',
            Value::set((string) $story)
        );
    }
}
