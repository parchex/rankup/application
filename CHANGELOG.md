# Changelog

## 0.1.8 - 2023-08-19

- Update project make tools to new version

## v0.1.7 - 2021-10-14

- Update tools and config

## v0.1.3 - 2020-12-01

- Update dependencies

## v0.1.2 - 2020-11-30

- Ranking Domain
  * Create ranking
  * Modify ranking
- Card Domain
  * Make a card
  * Modify card
- Candidate Domain
  * Enroll a candidate in Ranking
  * Enroll a candidate from a Card in Ranking
  * Unsubscribe Candidate
  * Modify Candidate content
