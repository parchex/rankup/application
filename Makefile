MKUP_INSTALL_SETUP = https://gitlab.com/oxkhar/makeup/raw/master/install.sh
MKUP_INSTALL_PATH  = .makeUp/MakeUp.mk

### Components config selection
## More info on https://gitlab.com/oxkhar/makeup/blob/master/example/Makefile
MAKEUP_HAS += docker-engine git-trunk
MAKEUP_HAS += php-composer php-tests-xunit php-tests-mutant php-qa
### Tag rules to update files with new version
TAG_APP_FILES += ./.env
./.env.tag: TAG_REG_EXP=/APP_VERSION=.*/APP_VERSION=${TAG}/

$(shell [ ! -e "${MKUP_INSTALL_PATH}" ] && curl -sSL "${MKUP_INSTALL_SETUP}" | bash)
include ${MKUP_INSTALL_PATH}
