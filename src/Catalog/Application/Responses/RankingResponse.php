<?php declare(strict_types=1);

namespace RankUp\Catalog\Application\Responses;

use Parchex\Core\Application\Response\Item;
use RankUp\Catalog\Domain\Ranking;

final class RankingResponse extends Item
{
    public function __construct(string $rankingId, string $title, string $description)
    {
        parent::__construct(
            [
                'ranking_id' => $rankingId,
                'title' => $title,
                'description' => $description,
            ]
        );
    }

    public static function from(Ranking $ranking): self
    {
        return new static(
            (string) $ranking->rankingId(),
            $ranking->title(),
            $ranking->description()
        );
    }
}
