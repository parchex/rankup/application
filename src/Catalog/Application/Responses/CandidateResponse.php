<?php declare(strict_types=1);

namespace RankUp\Catalog\Application\Responses;

use Parchex\Core\Application\Response\Item;
use RankUp\Catalog\Domain\Candidate;

final class CandidateResponse extends Item
{
    public function __construct(
        string $candidateId,
        string $cardId,
        string $title,
        string $presentation
    ) {
        parent::__construct(
            [
                'candidate_id' => $candidateId,
                'card_id' => $cardId,
                'title' => $title,
                'presentation' => $presentation,
            ]
        );
    }

    public static function from(Candidate $candidate): self
    {
        return new static(
            (string) $candidate->candidateId(),
            (string) $candidate->cardId(),
            $candidate->title(),
            $candidate->presentation()
        );
    }
}
