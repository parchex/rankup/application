<?php declare(strict_types=1);

namespace RankUp\Catalog\Application;

use Parchex\Core\Application\CommandHandler;
use RankUp\Catalog\Application\Responses\CardResponse;
use RankUp\Catalog\Domain\Card;
use RankUp\Catalog\Domain\CardId;
use RankUp\Catalog\Domain\CardRepository;

class CreateCardHandler implements CommandHandler
{
    /**
     * @var CardRepository
     */
    private $cardRepository;

    public function __construct(CardRepository $cardRepository)
    {
        $this->cardRepository = $cardRepository;
    }

    public function handle(CreateCardCommand $command): CardResponse
    {
        $card = Card::archive(
            CardId::generate(),
            $command->title(),
            $command->summary(),
            $command->story()
        );

        $this->cardRepository->add($card);

        return CardResponse::from($card);
    }
}
