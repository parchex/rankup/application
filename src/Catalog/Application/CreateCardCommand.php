<?php declare(strict_types=1);

namespace RankUp\Catalog\Application;

use Parchex\Core\Application\Command;

final class CreateCardCommand implements Command
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $summary;
    /**
     * @var string
     */
    private $story;

    public function __construct(string $title, string $summary, string $story)
    {
        $this->title = $title;
        $this->summary = $summary;
        $this->story = $story;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function summary(): string
    {
        return $this->summary;
    }

    public function story(): string
    {
        return $this->story;
    }
}
