<?php declare(strict_types=1);

namespace RankUp\Catalog\Application;

use Parchex\Core\Application\Command;

class ModifyCandidateContentCommand implements Command
{
    /**
     * @var string
     */
    private $candidateId;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $presentation;

    public function __construct(string $candidateId, string $title, string $presentation)
    {
        $this->candidateId = $candidateId;
        $this->title = $title;
        $this->presentation = $presentation;
    }

    public function candidateId(): string
    {
        return $this->candidateId;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function presentation(): string
    {
        return $this->presentation;
    }
}
