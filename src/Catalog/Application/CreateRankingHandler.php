<?php declare(strict_types=1);

namespace RankUp\Catalog\Application;

use Parchex\Core\Application\CommandHandler;
use RankUp\Catalog\Application\Responses\RankingResponse;
use RankUp\Catalog\Domain\Ranking;
use RankUp\Catalog\Domain\RankingId;
use RankUp\Catalog\Domain\RankingRepository;

class CreateRankingHandler implements CommandHandler
{
    /**
     * @var RankingRepository
     */
    private $rankingRepository;

    public function __construct(RankingRepository $rankingRepository)
    {
        $this->rankingRepository = $rankingRepository;
    }

    public function handle(CreateRankingCommand $command): RankingResponse
    {
        $rankingId = RankingId::generate();

        $ranking = Ranking::create($rankingId, $command->title(), $command->description());

        $this->rankingRepository->add($ranking);

        return RankingResponse::from($ranking);
    }
}
