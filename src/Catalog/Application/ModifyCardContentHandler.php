<?php declare(strict_types=1);

namespace RankUp\Catalog\Application;

use Parchex\Core\Application\CommandHandler;
use RankUp\Catalog\Application\Responses\CardResponse;
use RankUp\Catalog\Domain\CardId;
use RankUp\Catalog\Domain\CardRepository;

class ModifyCardContentHandler implements CommandHandler
{
    /**
     * @var CardRepository
     */
    private $repo;

    public function __construct(CardRepository $repo)
    {
        $this->repo = $repo;
    }

    public function handle(ModifyCardContentCommand $command): CardResponse
    {
        $card = $this->repo->get(CardId::fromString($command->cardId()));

        $card->modifyContent($command->title(), $command->summary());

        return CardResponse::from($card);
    }
}
