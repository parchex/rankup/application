<?php declare(strict_types=1);

namespace RankUp\Catalog\Application;

use Parchex\Core\Application\CommandHandler;
use RankUp\Catalog\Application\Responses\RankingResponse;
use RankUp\Catalog\Domain\RankingId;
use RankUp\Catalog\Domain\RankingRepository;

class ModifyRankingContentHandler implements CommandHandler
{
    /**
     * @var RankingRepository
     */
    private $repo;

    public function __construct(RankingRepository $repo)
    {
        $this->repo = $repo;
    }

    public function handle(ModifyRankingContentCommand $command): RankingResponse
    {
        $ranking = $this->repo->get(RankingId::fromString($command->rankingId()));

        $ranking->modifyContentInfo($command->title(), $command->description());

        return RankingResponse::from($ranking);
    }
}
