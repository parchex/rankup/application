<?php declare(strict_types=1);

namespace RankUp\Catalog\Application;

use Parchex\Core\Application\Command;

class ModifyCardContentCommand implements Command
{
    /**
     * @var string
     */
    private $cardId;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $summary;

    public function __construct(string $cardId, string $title, string $summary)
    {
        $this->cardId = $cardId;
        $this->title = $title;
        $this->summary = $summary;
    }

    public function cardId(): string
    {
        return $this->cardId;
    }

    public function summary(): string
    {
        return $this->summary;
    }

    public function title(): string
    {
        return $this->title;
    }
}
