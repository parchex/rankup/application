<?php declare(strict_types=1);

namespace RankUp\Catalog\Application;

use Parchex\Core\Application\Command;

final class EnrollCandidateInRankingCommand implements Command
{
    /**
     * @var string
     */
    private $rankingId;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $presentation;

    public function __construct(string $rankingId, string $title, string $presentation)
    {
        $this->rankingId = $rankingId;
        $this->title = $title;
        $this->presentation = $presentation;
    }

    public function rankingId(): string
    {
        return $this->rankingId;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function presentation(): string
    {
        return $this->presentation;
    }
}
