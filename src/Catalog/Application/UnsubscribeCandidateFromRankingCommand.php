<?php declare(strict_types=1);

namespace RankUp\Catalog\Application;

use Parchex\Core\Application\Command;

final class UnsubscribeCandidateFromRankingCommand implements Command
{
    /**
     * @var string
     */
    private $rankingId;
    /**
     * @var string
     */
    private $candidateId;

    public function __construct(string $rankingId, string $candidateId)
    {
        $this->rankingId = $rankingId;
        $this->candidateId = $candidateId;
    }

    public function rankingId(): string
    {
        return $this->rankingId;
    }

    public function candidateId(): string
    {
        return $this->candidateId;
    }
}
