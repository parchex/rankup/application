<?php declare(strict_types=1);

namespace RankUp\Catalog\Application;

use Parchex\Core\Application\CommandHandler;
use RankUp\Catalog\Application\Responses\CandidateResponse;
use RankUp\Catalog\Domain\CandidateId;
use RankUp\Catalog\Domain\CandidateRepository;

class ModifyCandidateContentHandler implements CommandHandler
{
    /**
     * @var CandidateRepository
     */
    private $repo;

    public function __construct(CandidateRepository $repo)
    {
        $this->repo = $repo;
    }

    public function handle(ModifyCandidateContentCommand $command): CandidateResponse
    {
        $candidate = $this->repo->get(CandidateId::fromString($command->candidateId()));

        $candidate->modifyContent($command->title(), $command->presentation());

        return CandidateResponse::from($candidate);
    }
}
