<?php declare(strict_types=1);

namespace RankUp\Catalog\Application;

use Parchex\Core\Application\CommandHandler;
use RankUp\Catalog\Application\Responses\CandidateResponse;
use RankUp\Catalog\Domain\CandidateId;
use RankUp\Catalog\Domain\CandidateRepository;
use RankUp\Catalog\Domain\RankingId;
use RankUp\Catalog\Domain\RankingRepository;

class UnsubscribeCandidateFromRankingHandler implements CommandHandler
{
    /**
     * @var RankingRepository
     */
    private $rankingRepository;
    /**
     * @var CandidateRepository
     */
    private $candidateRepository;

    public function __construct(
        RankingRepository $rankingRepository,
        CandidateRepository $candidateRepository
    ) {
        $this->rankingRepository = $rankingRepository;
        $this->candidateRepository = $candidateRepository;
    }

    public function handle(UnsubscribeCandidateFromRankingCommand $command): CandidateResponse
    {
        $ranking = $this->rankingRepository->get(RankingId::fromString($command->rankingId()));
        $candidate =
            $this->candidateRepository->get(CandidateId::fromString($command->candidateId()));

        $ranking->unsubscribeCandidate($candidate);

        return CandidateResponse::from($candidate);
    }
}
