<?php declare(strict_types=1);

namespace RankUp\Catalog\Application;

use Parchex\Core\Application\Command;

class ModifyRankingContentCommand implements Command
{
    /**
     * @var string
     */
    private $rankingId;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $description;

    public function __construct(string $rankingId, string $title, string $description)
    {
        $this->rankingId = $rankingId;
        $this->title = $title;
        $this->description = $description;
    }

    public function rankingId(): string
    {
        return $this->rankingId;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function description(): string
    {
        return $this->description;
    }
}
