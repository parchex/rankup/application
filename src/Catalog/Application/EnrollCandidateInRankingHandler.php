<?php declare(strict_types=1);

namespace RankUp\Catalog\Application;

use Parchex\Core\Application\CommandHandler;
use RankUp\Catalog\Application\Responses\CandidateResponse;
use RankUp\Catalog\Domain\CandidateId;
use RankUp\Catalog\Domain\Card;
use RankUp\Catalog\Domain\CardId;
use RankUp\Catalog\Domain\CardRepository;
use RankUp\Catalog\Domain\RankingId;
use RankUp\Catalog\Domain\RankingRepository;

class EnrollCandidateInRankingHandler implements CommandHandler
{
    /**
     * @var RankingRepository
     */
    private $rankingRepository;
    /**
     * @var CardRepository
     */
    private $cardRepository;

    public function __construct(
        RankingRepository $rankingRepository,
        CardRepository $cardRepository
    ) {
        $this->rankingRepository = $rankingRepository;
        $this->cardRepository = $cardRepository;
    }

    public function handle(EnrollCandidateInRankingCommand $command): CandidateResponse
    {
        $card = $this->createCard($command);

        $ranking = $this->rankingRepository
            ->get(RankingId::fromString($command->rankingId()));

        $candidate = $ranking->enrollCandidate(
            CandidateId::generate(),
            $card->cardId(),
            $command->title(),
            $command->presentation()
        );

        return Responses\CandidateResponse::from($candidate);
    }

    private function createCard(EnrollCandidateInRankingCommand $command): Card
    {
        $card = Card::archive(
            CardId::generate(),
            $command->title(),
            $command->presentation()
        );

        $this->cardRepository->add($card);

        return $card;
    }
}
