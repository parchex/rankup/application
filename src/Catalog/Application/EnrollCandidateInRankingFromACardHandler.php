<?php declare(strict_types=1);

namespace RankUp\Catalog\Application;

use Parchex\Core\Application\CommandHandler;
use RankUp\Catalog\Application\Responses\CandidateResponse;
use RankUp\Catalog\Domain\CandidateId;
use RankUp\Catalog\Domain\CardId;
use RankUp\Catalog\Domain\CardRepository;
use RankUp\Catalog\Domain\RankingId;
use RankUp\Catalog\Domain\RankingRepository;

class EnrollCandidateInRankingFromACardHandler implements CommandHandler
{
    /**
     * @var RankingRepository
     */
    private $rankingRepository;
    /**
     * @var CardRepository
     */
    private $cardRepository;

    public function __construct(
        RankingRepository $rankingRepository,
        CardRepository $cardRepository
    ) {
        $this->rankingRepository = $rankingRepository;
        $this->cardRepository = $cardRepository;
    }

    public function handle(EnrollCandidateInRankingFromACardCommand $command): CandidateResponse
    {
        return CandidateResponse::from(
            $this->rankingRepository
                ->get(RankingId::fromString($command->rankingId()))
                ->enrollCandidateFromCard(
                    CandidateId::generate(),
                    $this->cardRepository->get(CardId::fromString($command->cardId()))
                )
        );
    }
}
