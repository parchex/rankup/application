<?php declare(strict_types=1);

namespace RankUp\Catalog\Application;

use Parchex\Core\Application\Command;

final class EnrollCandidateInRankingFromACardCommand implements Command
{
    /**
     * @var string
     */
    private $rankingId;
    /**
     * @var string
     */
    private $cardId;

    public function __construct(string $rankingId, string $cardId)
    {
        $this->rankingId = $rankingId;
        $this->cardId = $cardId;
    }

    public function rankingId(): string
    {
        return $this->rankingId;
    }

    public function cardId(): string
    {
        return $this->cardId;
    }
}
