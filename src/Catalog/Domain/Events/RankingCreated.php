<?php declare(strict_types=1);

namespace RankUp\Catalog\Domain\Events;

use Parchex\Core\Domain\DomainEvent;
use RankUp\Catalog\Domain\Ranking;

final class RankingCreated extends DomainEvent
{
    private function __construct(
        string $rankingId,
        string $title,
        string $description,
        int $maximumCandidates
    ) {
        parent::__construct(
            [
                'ranking_id' => $rankingId,
                'title' => $title,
                'description' => $description,
                'maximum_candidates' => $maximumCandidates,
            ]
        );
    }

    public static function from(Ranking $ranking): self
    {
        return new self(
            (string) $ranking->rankingId(),
            $ranking->title(),
            $ranking->description(),
            $ranking->maximumCandidates()
        );
    }
}
