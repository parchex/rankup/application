<?php declare(strict_types=1);

namespace RankUp\Catalog\Domain\Events;

use Parchex\Core\Domain\DomainEvent;
use RankUp\Catalog\Domain\Candidate;
use RankUp\Catalog\Domain\Ranking;

final class CandidateInRankingEnrolled extends DomainEvent
{
    private function __construct(string $rankingId, string $candidateId, string $cardId)
    {
        parent::__construct(
            [
                'ranking_id' => $rankingId,
                'candidate_id' => $candidateId,
                'card_id' => $cardId,
            ]
        );
    }

    public static function from(Ranking $ranking, Candidate $candidate): self
    {
        return new static(
            (string) $ranking->rankingId(),
            (string) $candidate->candidateId(),
            (string) $candidate->cardId()
        );
    }
}
