<?php declare(strict_types=1);

namespace RankUp\Catalog\Domain\Events;

use Parchex\Core\Domain\DomainEvent;
use RankUp\Catalog\Domain\Candidate;
use RankUp\Catalog\Domain\Ranking;

class CandidateFromRankingUnsubscribed extends DomainEvent
{
    private function __construct(string $rankingId, string $candidateId)
    {
        parent::__construct(
            [
                'ranking_id' => $rankingId,
                'candidate_id' => $candidateId,
            ]
        );
    }

    public static function from(Ranking $ranking, Candidate $candidate): self
    {
        return new self((string) $ranking->rankingId(), (string) $candidate->candidateId());
    }
}
