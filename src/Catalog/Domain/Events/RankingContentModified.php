<?php declare(strict_types=1);

namespace RankUp\Catalog\Domain\Events;

use Parchex\Core\Domain\DomainEvent;
use RankUp\Catalog\Domain\Ranking;

class RankingContentModified extends DomainEvent
{
    private function __construct(string $rankingId, string $title, string $description)
    {
        parent::__construct(
            [
                'rankingId' => $rankingId,
                'title' => $title,
                'description' => $description,
            ]
        );
    }

    public static function from(Ranking $ranking): self
    {
        return new self((string) $ranking->rankingId(), $ranking->title(), $ranking->description());
    }
}
