<?php declare(strict_types=1);

namespace RankUp\Catalog\Domain\Events;

use Parchex\Core\Domain\DomainEvent;
use RankUp\Catalog\Domain\Card;

final class CardContentModified extends DomainEvent
{
    private function __construct(string $cardId, string $title, string $summary)
    {
        parent::__construct(
            [
                'card_id' => $cardId,
                'title' => $title,
                'summary' => $summary,
            ]
        );
    }

    public static function from(Card $card): self
    {
        return new static((string) $card->cardId(), $card->title(), $card->summary());
    }
}
