<?php declare(strict_types=1);

namespace RankUp\Catalog\Domain\Events;

use Parchex\Core\Domain\DomainEvent;
use RankUp\Catalog\Domain\Card;

final class CardStoryChanged extends DomainEvent
{
    private function __construct(string $cardId, string $story)
    {
        parent::__construct(
            [
                'card_id' => $cardId,
                'story' => $story,
            ]
        );
    }

    public static function from(Card $card): self
    {
        return new static((string) $card->cardId(), $card->story());
    }
}
