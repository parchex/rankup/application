<?php declare(strict_types=1);

namespace RankUp\Catalog\Domain;

use Parchex\Core\Domain\Repository;

interface RankingRepository extends Repository
{
    public function get(RankingId $rankingId): Ranking;

    public function add(Ranking $ranking): void;
}
