<?php declare(strict_types=1);

namespace RankUp\Catalog\Domain;

use Parchex\Core\Domain\Repository;

interface CandidateRepository extends Repository
{
    public function get(CandidateId $candidateId): Candidate;
}
