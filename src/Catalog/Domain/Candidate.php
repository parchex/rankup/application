<?php declare(strict_types=1);

namespace RankUp\Catalog\Domain;

use Parchex\Common\Assertion;
use Parchex\Core\Domain\Entity;
use Parchex\Core\Domain\Identifier;

class Candidate extends Entity
{
    /**
     * @var CandidateId
     */
    private $candidateId;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $presentation;
    /**
     * @var CardId
     */
    private $cardId;

    private function __construct(
        CandidateId $candidateId,
        CardId $cardId,
        string $title,
        string $presentation
    ) {
        $this->modifyContent($title, $presentation);

        $this->candidateId = $candidateId;
        $this->cardId = $cardId;
    }

    public static function makeFromCard(CandidateId $candidateId, Card $card): self
    {
        return new self($candidateId, $card->cardId(), $card->title(), $card->summary());
    }

    public static function make(
        CandidateId $candidateId,
        CardId $cardId,
        string $title,
        string $presentation
    ): self {
        return new self($candidateId, $cardId, $title, $presentation);
    }

    public function modifyContent(string $title, string $presentation): void
    {
        Assertion::notEmpty($title);
        Assertion::notEmpty($presentation);

        $this->title = $title;
        $this->presentation = $presentation;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function presentation(): string
    {
        return $this->presentation;
    }

    public function cardId(): CardId
    {
        return $this->cardId;
    }

    public function identifier(): Identifier
    {
        return $this->candidateId();
    }

    public function candidateId(): CandidateId
    {
        return $this->candidateId;
    }
}
