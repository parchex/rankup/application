<?php declare(strict_types=1);

namespace RankUp\Catalog\Domain;

use Parchex\Core\Domain\Id\Uuid;

class CandidateId extends Uuid
{
}
