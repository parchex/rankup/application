<?php declare(strict_types=1);

namespace RankUp\Catalog\Domain\Exceptions;

use DomainException;
use RankUp\Catalog\Domain\Ranking;

class NotEnoughCapacityOfCandidates extends DomainException
{
    public function __construct(Ranking $ranking)
    {
        parent::__construct(
            $ranking . ' reaches maximum number of candidates (' .
            $ranking->maximumCandidates() . ') and cannot enroll more'
        );
    }
}
