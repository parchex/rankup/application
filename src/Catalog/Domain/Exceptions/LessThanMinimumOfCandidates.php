<?php declare(strict_types=1);

namespace RankUp\Catalog\Domain\Exceptions;

use DomainException;
use RankUp\Catalog\Domain\Ranking;

class LessThanMinimumOfCandidates extends DomainException
{
    public function __construct(Ranking $ranking)
    {
        parent::__construct(
            $ranking . ' has minimum number of candidates (' .
            Ranking::MINIMUM_CANDIDATES . ') and cannot unsubscribe more'
        );
    }
}
