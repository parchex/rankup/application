<?php declare(strict_types=1);

namespace RankUp\Catalog\Domain;

use Parchex\Common\ArrayCollection;
use Parchex\Common\Assertion;
use Parchex\Common\ValidationException;
use Parchex\Core\Domain\Aggregate;
use Parchex\Core\Domain\EventPublisher;
use Parchex\Core\Domain\Identifier;
use RankUp\Catalog\Domain\Events\CandidateFromRankingUnsubscribed;
use RankUp\Catalog\Domain\Events\CandidateInRankingEnrolled;
use RankUp\Catalog\Domain\Events\RankingContentModified;
use RankUp\Catalog\Domain\Events\RankingCreated;
use RankUp\Catalog\Domain\Exceptions\LessThanMinimumOfCandidates;
use RankUp\Catalog\Domain\Exceptions\NotEnoughCapacityOfCandidates;

class Ranking extends Aggregate
{
    public const MAXIMUM_CANDIDATES_DEFAULT = 10;
    public const MINIMUM_CANDIDATES = 2;
    /**
     * @var RankingId
     */
    private $rankingId;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $description;
    /**
     * @var ArrayCollection<Candidate>
     */
    private $candidates;
    /**
     * @var int
     */
    private $maximumCandidates;

    /**
     * {@inheritdoc}
     *
     * @param array<Candidate> $candidates
     *
     * @throws ValidationException
     */
    private function __construct(
        RankingId $rankingId,
        string $title,
        string $description,
        int $maximumCandidates,
        array $candidates = []
    ) {
        $this->putContent($title, $description);
        Assertion::min($maximumCandidates, self::MINIMUM_CANDIDATES);

        $this->rankingId = $rankingId;

        $this->maximumCandidates = $maximumCandidates;
        $this->candidates = new ArrayCollection($candidates, Candidate::class);
    }

    public static function create(
        RankingId $rankingId,
        string $title,
        string $description,
        int $maximumCandidates = self::MAXIMUM_CANDIDATES_DEFAULT
    ): self {
        $ranking = new self($rankingId, $title, $description, $maximumCandidates);

        EventPublisher::publish(RankingCreated::from($ranking));

        return $ranking;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return iterable<Candidate>
     */
    public function candidates(): iterable
    {
        return $this->candidates->getIterator();
    }

    public function maximumCandidates(): int
    {
        return $this->maximumCandidates;
    }

    public function identifier(): Identifier
    {
        return $this->rankingId();
    }

    public function rankingId(): RankingId
    {
        return $this->rankingId;
    }

    public function enrollCandidateFromCard(CandidateId $candidateId, Card $card): Candidate
    {
        return $this->enroll(Candidate::makeFromCard($candidateId, $card));
    }

    public function enrollCandidate(
        CandidateId $candidateId,
        CardId $cardId,
        string $title,
        string $presentation
    ): Candidate {
        return $this->enroll(Candidate::make($candidateId, $cardId, $title, $presentation));
    }

    public function modifyContentInfo(string $title, string $description): void
    {
        $this->putContent($title, $description);

        EventPublisher::publish(RankingContentModified::from($this));
    }

    public function unsubscribeCandidate(Candidate $candidate): void
    {
        $this->assertNotLessMinimumCandidates();

        $this->candidates->removeElement($candidate);

        EventPublisher::publish(CandidateFromRankingUnsubscribed::from($this, $candidate));
    }

    private function assertEnoughCapacityOfCandidates(): void
    {
        if ($this->candidates->count() >= $this->maximumCandidates) {
            throw new NotEnoughCapacityOfCandidates($this);
        }
    }

    private function putContent(string $title, string $description): void
    {
        Assertion::notEmpty($title);
        Assertion::notEmpty($description);

        $this->title = $title;
        $this->description = $description;
    }

    private function enroll(Candidate $candidate): Candidate
    {
        $this->assertEnoughCapacityOfCandidates();

        $this->candidates->add($candidate);

        EventPublisher::publish(CandidateInRankingEnrolled::from($this, $candidate));

        return $candidate;
    }

    private function assertNotLessMinimumCandidates(): void
    {
        if ($this->candidates->count() === self::MINIMUM_CANDIDATES) {
            throw new LessThanMinimumOfCandidates($this);
        }
    }
}
