<?php declare(strict_types=1);

namespace RankUp\Catalog\Domain;

use Parchex\Core\Domain\Repository;

interface CardRepository extends Repository
{
    public function add(Card $card): void;

    public function get(CardId $cardId): Card;
}
