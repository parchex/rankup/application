<?php declare(strict_types=1);

namespace RankUp\Catalog\Domain;

use Parchex\Common\Assertion;
use Parchex\Core\Domain\Entity;
use Parchex\Core\Domain\EventPublisher;
use Parchex\Core\Domain\Identifier;
use RankUp\Catalog\Domain\Events\CardArchived;
use RankUp\Catalog\Domain\Events\CardContentModified;
use RankUp\Catalog\Domain\Events\CardStoryChanged;

class Card extends Entity
{
    /**
     * @var CardId
     */
    private $cardId;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $summary;
    /**
     * @var string
     */
    private $story;

    private function __construct(
        CardId $cardId,
        string $title,
        string $summary,
        string $story = ''
    ) {
        $this->putContent($title, $summary);
        $this->putStory($story);

        $this->cardId = $cardId;
        $this->title = $title;
        $this->summary = $summary;
    }

    public static function archiveCandidate(CardId $cardId, Candidate $candidate): self
    {
        return static::archive($cardId, $candidate->title(), $candidate->presentation());
    }

    public static function archive(
        CardId $cardId,
        string $title,
        string $summary,
        string $story = ''
    ): self {
        $card = new self($cardId, $title, $summary, $story);

        EventPublisher::publish(CardArchived::from($card));

        return $card;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function summary(): string
    {
        return $this->summary;
    }

    public function story(): string
    {
        return $this->story;
    }

    public function identifier(): Identifier
    {
        return $this->cardId();
    }

    public function cardId(): CardId
    {
        return $this->cardId;
    }

    public function modifyContent(string $title, string $summary): void
    {
        $this->putContent($title, $summary);

        EventPublisher::publish(CardContentModified::from($this));
    }

    public function changeStory(string $story): void
    {
        $this->putStory($story);

        EventPublisher::publish(CardStoryChanged::from($this));
    }

    private function putContent(string $title, string $summary): void
    {
        Assertion::notEmpty($title);
        Assertion::notEmpty($summary);

        $this->title = $title;
        $this->summary = $summary;
    }

    private function putStory(string $story): void
    {
        $this->story = $story;
    }
}
