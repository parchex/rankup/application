<?php declare(strict_types=1);

namespace RankUp\Catalog\Infrastructure\Persistence;

use Parchex\Core\Infrastructure\InMemoryRepository;
use RankUp\Catalog\Domain\Ranking;
use RankUp\Catalog\Domain\RankingId;
use RankUp\Catalog\Domain\RankingRepository;

class InMemoryRankingRepository extends InMemoryRepository implements RankingRepository
{
    /**
     * @param array<Ranking> $entities
     */
    public function __construct(array $entities = [])
    {
        parent::__construct(Ranking::class, $entities);
    }

    public function get(RankingId $rankingId): Ranking
    {
        return $this->storage()->get($rankingId);
    }

    public function add(Ranking $ranking): void
    {
        $this->storage()->set($ranking);
    }
}
