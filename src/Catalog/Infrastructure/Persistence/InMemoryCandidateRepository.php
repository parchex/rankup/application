<?php declare(strict_types=1);

namespace RankUp\Catalog\Infrastructure\Persistence;

use Parchex\Core\Infrastructure\InMemoryRepository;
use RankUp\Catalog\Domain\Candidate;
use RankUp\Catalog\Domain\CandidateId;
use RankUp\Catalog\Domain\CandidateRepository;

class InMemoryCandidateRepository extends InMemoryRepository implements CandidateRepository
{
    /**
     * {@inheritdoc}
     *
     * @param array<Candidate> $entities
     */
    public function __construct(array $entities = [])
    {
        parent::__construct(Candidate::class, $entities);
    }

    public function get(CandidateId $candidateId): Candidate
    {
        return $this->storage()->get($candidateId);
    }
}
