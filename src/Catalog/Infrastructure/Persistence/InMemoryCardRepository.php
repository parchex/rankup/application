<?php declare(strict_types=1);

namespace RankUp\Catalog\Infrastructure\Persistence;

use Parchex\Core\Infrastructure\InMemoryRepository;
use RankUp\Catalog\Domain\Card;
use RankUp\Catalog\Domain\CardId;
use RankUp\Catalog\Domain\CardRepository;

class InMemoryCardRepository extends InMemoryRepository implements CardRepository
{
    /**
     * @param array<Card> $entities
     *
     * {@inheritdoc}
     */
    public function __construct(array $entities = [])
    {
        parent::__construct(Card::class, $entities);
    }

    public function add(Card $card): void
    {
        $this->storage()->set($card);
    }

    public function get(CardId $cardId): Card
    {
        return $this->storage()->get($cardId);
    }
}
