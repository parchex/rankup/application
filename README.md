# RankUp Application - Ranking for the bests

_Párrafo que describa lo que es el proyecto_

Instalar el proyecto descargando el repositorio de git...

```
git clone git@gitlab.com:parchex/rankup/application
``` 

## Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

### PHP

Se necesitará tener "**PHP cli**" instalado, bien en local o virtualizado con docker.

Para su ejecución necesitamos **PHP 7.3** o superior en su versión de línea de comandos,
bien instalada en local o en una imagen Docker.

Desde el proyecto se puede generar una imagen de Docker con todo lo necesario para su ejecución.
En este caso debes [tener instalado Docker](https://docs.docker.com/engine/installation/)

### Make

Para simplificar las tareas de desarrollo se han definido unas tareas que se ejecutan 
con el comando **make**, revisa que dispones de este comando en tu sistema y que puedes
lanzarlo desde el directorio del proyecto…

```
# Muestra las tareas del proyecto
make help
```

## Instalación 🔧

Inicializar el proyecto ejecutando teniendo **PHP** en local... 

```
make install-dev-env
```

Con esto tendríamos desplegado el proyecto con las dependencias y herramientas
necesarias para su ejecución...

* Utilidades necesarias para desarrollo y pruebas en el directorio `bin`...
  * composer
  * testing tools (*phpunit*, *behat*, ...)
* Dependencias instaladas (*composer*)
* Configuración del entorno (fichero `.env`)

---
Si queremos trabajar en un entorno virtualizado con Docker 
y poder ejecutar la aplicación con su imagen específica...

```
make install-docker-env
```
... con esto dispondremos de todo lo anterior de una instalación local,
más la imagen de Docker necesaria.

---
Sí quisiéramos borrar la instalación generada podemos 
recurrir ejecutando `make uninstall`

### Herramientas

Aparte del comando `make`  existe el fichero `aliases.sh` que contiene una serie de comandos 
para ejecutar las herramientas instaladas en el proyecto...

```bash
# Para instalar los comandos...
. aliases.sh
```

* **php-cli**: Podemos ejecutar cualquier script PHP con la versión PHP del proyecto.

```bash
php-cli public/index.php
php-cli tests/FileTests.php
```

* Utilidades de desarrollo

```bash
# Para tests unitarios
phpunit
phpunit-coverage
# Verificar estilo código
phpstan
```
* **composer**: lanza composer usando la version de PHP del contenedor de la aplicación
```bash
. aliases.sh

composer update
composer show
```

* También incluye comandos para simplificar la ejecución de Docker con el proyecto.
```bash
# Ejecución contenedores con la imagen del proyecto
docker-run
```

## Ejecutando las pruebas ⚙️

Para comprobar si se pasan los tests...

```bash
make test
```
... y si queremos ver algo más de información ...

```bash
make test-verbose
make test-coverage
```
... podemos generar un completo informe de los tests con ...

```bash
make tests-report
```

### Y las pruebas de estilo de codificación ⌨️

Un completo análisis del código utilizando diversas herramientas de revisión y métricas con...

```
make analyse
```

## Construido con 🛠️

* [Docker](https://www.docker.com/) - Virtualización
* [Composer](https://getcomposer.org/) - Gestor de dependencias
* [PHPUnit](https://phpunit.de/) - xUnit testing framework
* [Parchex](https://gitlab.com/parchex/basics) - Desarrollo de aplicaciones basadas en DDD/Hexagonal Arch.

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. 
Para todas las versiones disponibles, mira los [tags en este repositorio](https://gitlab.com/parchex/rankup/application).

## Licencia 📄

Este proyecto está bajo la Licencia (WTFPL) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

---
⌨️ con ❤️ por [Oxkhar](https://oxkhar.com/) 😋
