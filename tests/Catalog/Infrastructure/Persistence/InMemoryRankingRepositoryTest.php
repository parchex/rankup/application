<?php declare(strict_types=1);

namespace Tests\RankUp\Catalog\Infrastructure\Persistence;

use Fixtures\RankUp\Catalog\RankingProvider;
use PHPUnit\Framework\TestCase;
use RankUp\Catalog\Infrastructure\Persistence\InMemoryRankingRepository;

/**
 * @testdox Ranking Repository In Memory Implementation
 */
class InMemoryRankingRepositoryTest extends TestCase
{
    /**
     * @test
     */
    public function it_persists_ranking(): void
    {
        $fixture = RankingProvider::ranking();
        $other = RankingProvider::ranking();

        $repo = new InMemoryRankingRepository(
            [
                RankingProvider::ranking(),
                RankingProvider::ranking(),
                $other,
                RankingProvider::ranking(),
            ]
        );

        $repo->add($fixture);

        $ranking = $repo->get($fixture->rankingId());
        $otherRanking = $repo->get($other->rankingId());

        self::assertEquals($fixture, $ranking);
        self::assertEquals($other, $otherRanking);
    }
}
