<?php declare(strict_types=1);

namespace Tests\RankUp\Catalog\Infrastructure\Persistence;

use Fixtures\RankUp\Catalog\CandidateProvider;
use PHPUnit\Framework\TestCase;
use RankUp\Catalog\Infrastructure\Persistence\InMemoryCandidateRepository;

/**
 * @testdox Candidate Repository In Memory Implementation
 */
class InMemoryCandidateRepositoryTest extends TestCase
{
    /**
     * @test
     */
    public function it_accesses_candidates(): void
    {
        $fixture = CandidateProvider::candidate();
        $other = CandidateProvider::candidate();

        $repo = new InMemoryCandidateRepository(
            [
                CandidateProvider::candidate(),
                CandidateProvider::candidate(),
                $fixture,
                CandidateProvider::candidate(),
                $other,
            ]
        );

        $candidate = $repo->get($fixture->candidateId());
        $otherCandidate = $repo->get($other->candidateId());

        self::assertEquals($fixture, $candidate);
        self::assertEquals($other, $otherCandidate);
    }
}
