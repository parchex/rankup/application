<?php declare(strict_types=1);

namespace Tests\RankUp\Catalog\Infrastructure\Persistence;

use Fixtures\RankUp\Catalog\CardProvider;
use PHPUnit\Framework\TestCase;
use RankUp\Catalog\Infrastructure\Persistence\InMemoryCardRepository;

/**
 * @testdox Card Repository In Memory Implementation
 */
class InMemoryCardRepositoryTest extends TestCase
{
    /**
     * @test
     */
    public function it_persists_cards(): void
    {
        $repo = new InMemoryCardRepository();

        $fixture = CardProvider::card();
        $other = CardProvider::card();

        $repo->add($fixture);
        $repo->add($other);

        $card = $repo->get($fixture->cardId());
        $otherCard = $repo->get($other->cardId());

        self::assertEquals($fixture, $card);
        self::assertEquals($other, $otherCard);
    }
}
