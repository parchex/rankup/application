<?php declare(strict_types=1);

namespace Tests\RankUp\Catalog\Domain;

use Fixtures\RankUp\Catalog\CandidateProvider;
use Fixtures\RankUp\Catalog\CardBuilder;
use Parchex\Common\ValidationException;
use Parchex\Core\Domain\Entity;
use PHPUnit\Framework\TestCase;
use RankUp\Catalog\Domain\Card;
use RankUp\Catalog\Domain\CardId;
use RankUp\Catalog\Domain\Events\CardArchived;
use RankUp\Catalog\Domain\Events\CardContentModified;
use RankUp\Catalog\Domain\Events\CardStoryChanged;
use Tests\Parchex\PHPUnit\TestCases\EventTestCase;

/**
 * @testdox Card has detail and common info about candidates
 */
class CardTest extends TestCase
{
    use EventTestCase;

    /**
     * @test
     */
    public function it_is_a_entity(): void
    {
        self::assertTrue(is_a(Card::class, Entity::class, true));
    }

    /**
     * @test
     */
    public function it_archives_card_info(): void
    {
        //Given
        $fixtures = CardBuilder::create()->fixtures();
        // When
        $card = Card::archive(
            $fixtures['cardId'],
            $fixtures['title'],
            $fixtures['summary'],
            $fixtures['story']
        );
        // Then
        self::assertEquals($fixtures['cardId'], $card->cardId());
        self::assertEquals($fixtures['title'], $card->title());
        self::assertEquals($fixtures['summary'], $card->summary());
        self::assertEquals($fixtures['story'], $card->story());
        // And
        self::assertEventIsSubmitted(CardArchived::class);
    }

    /**
     * @test
     * @dataProvider invalidCardParametersProvider
     */
    public function it_fails_archive_card(array $invalidParams): void
    {
        self::expectException(ValidationException::class);

        Card::archive(
            $invalidParams["cardId"],
            $invalidParams["title"],
            $invalidParams["summary"],
            $invalidParams["story"]
        );
    }

    public function invalidCardParametersProvider()
    {
        $fixture = CardBuilder::create()->fixtures();

        yield "empty title" => [array_merge($fixture, ["title" => ""])];
        yield "empty summary" => [array_merge($fixture, ["summary" => ""])];
    }

    /**
     * @test
     */
    public function it_modifies_card_content(): void
    {
        //Given
        $content = [
            'title'   => "title modified",
            'summary' => "title modified",
        ];
        $card = CardBuilder::create()->build();
        // When
        $card->modifyContent($content['title'], $content['summary']);
        // Then
        self::assertEquals($content['title'], $card->title());
        self::assertEquals($content['summary'], $card->summary());
        // And
        self::assertEventIsSubmitted(CardContentModified::class);
    }

    /**
     * @test
     */
    public function it_changes_the_story_of_card(): void
    {
        //Given
        $story = "story modified";
        $card = CardBuilder::create()->build();
        // When
        $card->changeStory($story);
        // Then
        self::assertEquals($story, $card->story());
        // And
        self::assertEventIsSubmitted(CardStoryChanged::class);
    }

    /**
     * @test
     */
    public function it_archives_card_with_info_from_a_candidate(): void
    {
        //Given
        $cardId = CardId::generate();
        $candidate = CandidateProvider::candidate();
        // When
        $card = Card::archiveCandidate($cardId, $candidate);
        // Then
        self::assertEquals($cardId, $card->cardId());
        self::assertEquals($candidate->title(), $card->title());
        self::assertEquals($candidate->presentation(), $card->summary());
    }
}
