<?php declare(strict_types=1);

namespace Tests\RankUp\Catalog\Domain;

use Fixtures\RankUp\Catalog\CandidateBuilder;
use Fixtures\RankUp\Catalog\CandidateProvider;
use Fixtures\RankUp\Catalog\CardProvider;
use Parchex\Common\ValidationException;
use Parchex\Core\Domain\Entity;
use PHPUnit\Framework\TestCase;
use RankUp\Catalog\Domain\Candidate;
use RankUp\Catalog\Domain\CandidateId;
use RankUp\Catalog\Domain\CardId;
use Tests\Parchex\PHPUnit\TestCases\EventTestCase;

/**
 * @testdox Candidate listed in a ranking
 */
class CandidateTest extends TestCase
{
    use EventTestCase;

    /**
     * @test
     */
    public function it_is_a_entity(): void
    {
        self::assertTrue(is_a(Candidate::class, Entity::class, true));
    }

    /**
     * @test
     */
    public function it_makes_a_candidate_with_a_card_reference(): void
    {
        // Given
        $cardId = CardId::generate();
        $fixture = CandidateBuilder::create()->fixtures();

        // When
        $candidate = Candidate::make(
            $fixture['candidateId'],
            $cardId,
            $fixture['title'],
            $fixture['presentation'],
        );

        // Then
        self::assertEquals($cardId, $candidate->cardId());
        self::assertEquals($fixture['candidateId'], $candidate->candidateId());
        self::assertEquals($fixture['title'], $candidate->title());
        self::assertEquals($fixture['presentation'], $candidate->presentation());
    }

    /**
     * @test
     */
    public function it_makes_a_candidate_from_a_card(): void
    {
        // Given
        $candidateId = CandidateId::generate();
        $card = CardProvider::card();

        // When
        $candidate = Candidate::makeFromCard($candidateId, $card);

        // Then
        self::assertEquals($card->cardId(), $candidate->cardId());
        self::assertEquals($card->title(), $candidate->title());
        self::assertEquals($card->summary(), $candidate->presentation());
    }

    /**
     * @test
     * @dataProvider invalidCandidateParametersProvider
     */
    public function it_fails_making_a_candidate(array $invalidParams): void
    {
        self::expectException(ValidationException::class);

        Candidate::make(
            $invalidParams["candidateId"],
            $invalidParams["cardId"],
            $invalidParams["title"],
            $invalidParams["presentation"]
        );
    }

    public function invalidCandidateParametersProvider()
    {
        $fixture = CandidateBuilder::create()->fixtures();

        yield "empty title" => [array_merge($fixture, ["title" => ""])];
        yield "empty presentation" => [array_merge($fixture, ["presentation" => ""])];
    }

    /**
     * @test
     */
    public function it_modifies_candidate_content(): void
    {
        // Given
        $candidate = CandidateProvider::candidate();
        $fixture = CandidateBuilder::create()->fixtures();

        // When
        $candidate->modifyContent(
            $fixture['title'],
            $fixture['presentation'],
        );

        // Then
        self::assertEquals($fixture['title'], $candidate->title());
        self::assertEquals($fixture['presentation'], $candidate->presentation());
    }

    /**
     * @test
     * @dataProvider invalidCandidateParametersProvider
     */
    public function it_fails_updating_content_candidate(array $invalidParams): void
    {
        self::expectException(ValidationException::class);

        Candidate::make(
            $invalidParams["candidateId"],
            $invalidParams["cardId"],
            $invalidParams["title"],
            $invalidParams["presentation"]
        );
    }
}
