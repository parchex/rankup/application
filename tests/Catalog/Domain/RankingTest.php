<?php declare(strict_types=1);

namespace Tests\RankUp\Catalog\Domain;

use Fixtures\RankUp\Catalog\CandidateBuilder;
use Fixtures\RankUp\Catalog\CandidateProvider;
use Fixtures\RankUp\Catalog\CardProvider;
use Fixtures\RankUp\Catalog\RankingBuilder;
use Fixtures\RankUp\Catalog\RankingProvider;
use Parchex\Common\ValidationException;
use Parchex\Core\Domain\Entity;
use PHPUnit\Framework\TestCase;
use RankUp\Catalog\Domain\CandidateId;
use RankUp\Catalog\Domain\Events\CandidateFromRankingUnsubscribed;
use RankUp\Catalog\Domain\Events\CandidateInRankingEnrolled;
use RankUp\Catalog\Domain\Events\RankingContentModified;
use RankUp\Catalog\Domain\Events\RankingCreated;
use RankUp\Catalog\Domain\Exceptions\LessThanMinimumOfCandidates;
use RankUp\Catalog\Domain\Exceptions\NotEnoughCapacityOfCandidates;
use RankUp\Catalog\Domain\Ranking;
use Tests\Parchex\PHPUnit\TestCases\CollectionTestCase;
use Tests\Parchex\PHPUnit\TestCases\EventTestCase;

/**
 * @testdox Ranking of the bests
 */
class RankingTest extends TestCase
{
    use EventTestCase;
    use CollectionTestCase;

    /**
     * @test
     */
    public function it_is_a_entity(): void
    {
        self::assertTrue(is_a(Ranking::class, Entity::class, true));
    }

    /**
     * @test
     */
    public function it_creates_a_ranking(): void
    {
        // Given
        $fixture = RankingBuilder::create()->fixtures();
        // When
        $ranking =
            Ranking::create($fixture['rankingId'], $fixture['title'], $fixture['description']);
        // Then
        self::assertEquals($fixture['rankingId'], $ranking->rankingId());
        self::assertEquals($fixture['title'], $ranking->title());
        self::assertEquals($fixture['description'], $ranking->description());
        // And
        self::assertEventIsSubmitted(RankingCreated::class);
    }

    /**
     * @test
     */
    public function it_creates_a_ranking_with_default_capacity_of_candidates(): void
    {
        // Given
        $fixture = RankingBuilder::create()->fixtures();
        // When
        $ranking =
            Ranking::create($fixture['rankingId'], $fixture['title'], $fixture['description']);
        // Then
        self::assertEquals(Ranking::MAXIMUM_CANDIDATES_DEFAULT, $ranking->maximumCandidates());
    }

    /**
     * @test
     */
    public function it_creates_a_ranking_with_a_custom_capacity_of_candidates(): void
    {
        // Given
        $capacity = 17;
        $fixture = RankingBuilder::create()->fixtures();
        // When
        $ranking = Ranking::create(
            $fixture['rankingId'],
            $fixture['title'],
            $fixture['description'],
            $capacity
        );
        // Then
        self::assertEquals($capacity, $ranking->maximumCandidates());
    }

    /**
     * @test
     * @dataProvider invalidRankingParametersProvider
     */
    public function it_is_not_valid_ranking_creation($invalidParams): void
    {
        self::expectException(ValidationException::class);

        Ranking::create(
            $invalidParams['rankingId'],
            $invalidParams['title'],
            $invalidParams['description'],
            $invalidParams['maximumCandidates']
        );
    }

    public function invalidRankingParametersProvider()
    {
        $fixture = RankingBuilder::create()->fixtures();

        yield "title empty" => [array_merge($fixture, ["title" => ""])];
        yield "description empty" => [array_merge($fixture, ["description" => ""])];
        yield "max. candidates lower than 2" => [array_merge($fixture, ["maximumCandidates" => 1])];
    }

    /**
     * @test
     */
    public function it_enrolls_a_candidate_into_ranking(): void
    {
        // Given
        $ranking = RankingBuilder::create()
            ->withCandidates(
                [
                    CandidateProvider::candidate(),
                    CandidateProvider::candidate(),
                    CandidateProvider::candidate(),
                ]
            )
            ->withMaximumCandidates(10)
            ->build();

        $fixture = CandidateBuilder::create()->fixtures();
        // When
        $candidate = $ranking->enrollCandidate(
            $fixture['candidateId'],
            $fixture['cardId'],
            $fixture['title'],
            $fixture['presentation']
        );
        // Then
        self::assertEquals($fixture['candidateId'], $candidate->candidateId());
        self::assertEquals($fixture['cardId'], $candidate->cardId());
        self::assertEquals($fixture['title'], $candidate->title());
        self::assertEquals($fixture['presentation'], $candidate->presentation());
        // And
        self::assertContains(
            $candidate,
            $ranking->candidates(),
            'Candidate not enrolled in ranking'
        );
        // And
        self::assertEventIsSubmitted(CandidateInRankingEnrolled::class);
    }

    /**
     * @test
     */
    public function it_can_enroll_a_candidate_from_a_card(): void
    {
        // Given
        $ranking = RankingBuilder::create()
            ->withCandidates(
                [
                    CandidateProvider::candidate(),
                    CandidateProvider::candidate(),
                    CandidateProvider::candidate(),
                ]
            )
            ->withMaximumCandidates(10)
            ->build();

        $card = CardProvider::card();
        $candidateId = CandidateId::generate();

        $candidate = $ranking->enrollCandidateFromCard($candidateId, $card);

        self::assertEquals($candidateId, $candidate->candidateId());
        self::assertEquals($card->cardId(), $candidate->cardId());
        self::assertEquals($card->title(), $candidate->title());
        self::assertEquals($card->summary(), $candidate->presentation());
        // And
        self::assertContains(
            $candidate,
            $ranking->candidates(),
            'Candidate not enrolled in ranking'
        );
        // And
        self::assertEventIsSubmitted(CandidateInRankingEnrolled::class);
    }

    /**
     * @test
     */
    public function it_throws_exception_when_a_ranking_filled_with_maximum_capacity_of_candidates_enrolls_a_new_candidate(): void
    {
        $ranking = RankingBuilder::create()
            ->withCandidates(
                [
                    CandidateProvider::candidate(),
                    CandidateProvider::candidate(),
                    CandidateProvider::candidate(),
                    CandidateProvider::candidate(),
                    CandidateProvider::candidate(),
                ]
            )
            ->withMaximumCandidates(5)
            ->build();

        $fixture = CandidateBuilder::create()->fixtures();

        self::expectException(NotEnoughCapacityOfCandidates::class);
        self::expectExceptionMessageMatches('/.*reaches maximum number of candidates.*/');

        $ranking->enrollCandidate(
            $fixture['candidateId'],
            $fixture['cardId'],
            $fixture['title'],
            $fixture['presentation']
        );
    }

    /**
     * @test
     */
    public function it_unsubscribes_a_candidate(): void
    {
        // Given
        $candidate = CandidateProvider::candidate();
        $ranking = RankingBuilder::create()
            ->withCandidates(
                [
                    CandidateProvider::candidate(),
                    $candidate,
                    CandidateProvider::candidate(),
                    CandidateProvider::candidate(),
                ]
            )
            ->withMaximumCandidates(10)
            ->build();

        // When
        $ranking->unsubscribeCandidate($candidate);
        // Then
        self::assertCollectionNotContainsItem(
            $candidate,
            $ranking->candidates(),
            'Candidate not unsubscribed from ranking'
        );
        // And
        self::assertEventIsSubmitted(CandidateFromRankingUnsubscribed::class);
    }

    /**
     * @test
     */
    public function it_throws_exception_when_unsubscribed_candidate_from_ranking_with_minimum_candidates(): void
    {
        // Given
        $candidate = CandidateProvider::candidate();
        $candidates = CandidateProvider::candidates(Ranking::MINIMUM_CANDIDATES - 1);
        $candidates[] = $candidate;

        $ranking = RankingBuilder::create()
            ->withCandidates($candidates)
            ->withMaximumCandidates(10)
            ->build();

        // Expects
        self::expectException(LessThanMinimumOfCandidates::class);
        self::expectExceptionMessageMatches('/.*has minimum number of candidates*/');

        // When
        $ranking->unsubscribeCandidate($candidate);
    }

    /**
     * @test
     */
    public function it_modifies_content_info(): void
    {
        // Given
        $fixture = RankingBuilder::create()->fixtures();
        $ranking = RankingProvider::ranking();
        // When
        $ranking->modifyContentInfo($fixture['title'], $fixture['description']);
        // Then
        self::assertEquals($fixture['title'], $ranking->title());
        self::assertEquals($fixture['description'], $ranking->description());
        // And
        self::assertEventIsSubmitted(RankingContentModified::class);
    }
}
