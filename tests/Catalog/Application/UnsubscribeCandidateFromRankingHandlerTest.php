<?php declare(strict_types=1);

namespace Tests\RankUp\Catalog\Application;

use Fixtures\RankUp\Catalog\CandidateProvider;
use Fixtures\RankUp\Catalog\RankingBuilder;
use Fixtures\RankUp\Catalog\RankingProvider;
use PHPUnit\Framework\TestCase;
use RankUp\Catalog\Application\UnsubscribeCandidateFromRankingCommand;
use RankUp\Catalog\Application\UnsubscribeCandidateFromRankingHandler;
use RankUp\Catalog\Infrastructure\Persistence\InMemoryCandidateRepository;
use RankUp\Catalog\Infrastructure\Persistence\InMemoryRankingRepository;
use Tests\Parchex\PHPUnit\TestCases\CollectionTestCase;

/**
 * @testdox Command Unsubscribe Candidate From Ranking
 */
class UnsubscribeCandidateFromRankingHandlerTest extends TestCase
{
    use CollectionTestCase;

    /**
     * @test
     */
    public function it_gets_ranking_to_removes_candidate(): void
    {
        $candidates = CandidateProvider::candidates(11);
        $candidate = $candidates[5];

        $candidateRepo = new InMemoryCandidateRepository($candidates);
        $ranking = RankingBuilder::create()
            ->withCandidates(array_slice($candidates, 2, 5))
            ->build();
        $rankingRepo = new InMemoryRankingRepository(
            [
                RankingProvider::ranking(),
                RankingProvider::ranking(),
                $ranking,
                RankingProvider::ranking(),
                RankingProvider::ranking(),
            ]
        );

        $command = new UnsubscribeCandidateFromRankingCommand(
            (string) $ranking->rankingId(),
            (string) $candidate->candidateId()
        );

        $handler = new UnsubscribeCandidateFromRankingHandler($rankingRepo, $candidateRepo);

        $handler->handle($command);

        self::assertCollectionNotContainsItem($candidate, $ranking->candidates());
    }
}
