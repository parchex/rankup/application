<?php declare(strict_types=1);

namespace Tests\RankUp\Catalog\Application;

use Fixtures\RankUp\Catalog\CandidateBuilder;
use RankUp\Catalog\Application\EnrollCandidateInRankingCommand;
use RankUp\Catalog\Application\EnrollCandidateInRankingHandler;
use RankUp\Catalog\Application\Responses\CandidateResponse;
use RankUp\Catalog\Application\Responses\RankingCandidatesResponse;
use RankUp\Catalog\Domain\Candidate;
use RankUp\Catalog\Domain\Ranking;

/**
 * @testdox Command Enrolling Candidates in Ranking
 */
class EnrollCandidateInRankingHandlerTest extends RankingTestCase
{
    /**
     * @test
     */
    public function it_enrolls_the_candidate_in_ranking(): void
    {
        $ranking = $this->createRankingFixturesFillingWithCandidateFixtures();
        $fixture = CandidateBuilder::create()->fixtures();
        // Given
        $command = $this->givenACommandForEnrollACandidateInRanking($ranking, $fixture);
        $handler = $this->givenTheHandlerOfCommand();
        // When
        $response = $this->whenTheCommandIsHandled($handler, $command);
        // Then
        $candidate = $this->createExpectedCandidate($command, $response);
        self::assertCandidateIsEnrolledInRanking($ranking, $candidate);
    }

    /**
     * @test
     */
    public function it_creates_a_card_for_the_candidate(): void
    {
        $ranking = $this->createRankingFixturesFillingWithCandidateFixtures();
        $fixture = CandidateBuilder::create()->fixtures();
        // Given
        $command = $this->givenACommandForEnrollACandidateInRanking($ranking, $fixture);
        $handler = $this->givenTheHandlerOfCommand();
        // When
        $response = $this->whenTheCommandIsHandled($handler, $command);
        // Then
        $candidate = $this->createExpectedCandidate($command, $response);
        $card = $this->cardRepo->get($candidate->cardId());
        self::assertCardIsCreatedForCandidate($candidate, $card);
    }

    /**
     * @test
     */
    public function it_responses_candidate_info(): void
    {
        // Given
        $ranking = $this->createRankingFixturesFillingWithCandidateFixtures();
        $fixture = CandidateBuilder::create()->fixtures();

        $command = $this->givenACommandForEnrollACandidateInRanking($ranking, $fixture);
        $handler = $this->givenTheHandlerOfCommand();
        // When
        $response = $this->whenTheCommandIsHandled($handler, $command);
        // Then
        self::assertNotEmpty($response['card_id']);
        self::assertNotEmpty($response['candidate_id']);
        self::assertEquals($command->title(), $response['title']);
        self::assertEquals($command->presentation(), $response['presentation']);
    }

    protected static function givenACommandForEnrollACandidateInRanking(
        Ranking $ranking,
        array $fixture
    ): EnrollCandidateInRankingCommand {
        return new EnrollCandidateInRankingCommand(
            (string) $ranking->rankingId(),
            $fixture['title'],
            $fixture['presentation']
        );
    }

    protected function givenTheHandlerOfCommand(): EnrollCandidateInRankingHandler
    {
        return new EnrollCandidateInRankingHandler(
            $this->rankingRepo,
            $this->cardRepo
        );
    }

    protected function whenTheCommandIsHandled(
        EnrollCandidateInRankingHandler $handler,
        EnrollCandidateInRankingCommand $command
    ): CandidateResponse {
        return $handler->handle($command);
    }

    protected function createExpectedCandidate(
        EnrollCandidateInRankingCommand $command,
        CandidateResponse $response
    ): Candidate {
        return CandidateBuilder::create()
            ->withTitle($command->title())
            ->withPresentation($command->presentation())
            ->withCandidateId($response['candidate_id'])
            ->withCardId($response["card_id"])
            ->build();
    }
}
