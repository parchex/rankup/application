<?php declare(strict_types=1);

namespace Tests\RankUp\Catalog\Application;

use Fixtures\RankUp\Catalog\CardBuilder;
use Fixtures\RankUp\Catalog\CardProvider;
use PHPUnit\Framework\TestCase;
use RankUp\Catalog\Application\ModifyCardContentCommand;
use RankUp\Catalog\Application\ModifyCardContentHandler;
use RankUp\Catalog\Domain\CardId;
use RankUp\Catalog\Infrastructure\Persistence\InMemoryCardRepository;

/**
 * @testdox Modify Card Content Command
 */
class ModifyCardContentHandlerTest extends TestCase
{
    /**
     * @var InMemoryCardRepository
     */
    protected $repo;
    /**
     * @var ModifyCardContentHandler
     */
    protected $handler;
    /**
     * @var CardId
     */
    protected $cardId;

    protected function setUp(): void
    {
        $this->cardId = CardId::generate();

        $this->repo = new InMemoryCardRepository(
            [
                CardProvider::card(),
                CardProvider::card(),
                CardBuilder::create()->withCardId($this->cardId)->build(),
                CardProvider::card(),
                CardProvider::card(),
            ]
        );
        $this->handler = new ModifyCardContentHandler($this->repo);
    }

    /**
     * @test
     */
    public function it_gets_card_from_repository_and_modifies_content_from_command(): void
    {
        $fixture = CardBuilder::create()
            ->withCardId($this->cardId)
            ->fixtures();
        $command = new ModifyCardContentCommand(
            (string) $fixture['cardId'],
            $fixture['title'],
            $fixture['summary']
        );

        $this->handler->handle($command);

        $card = $this->repo->get($this->cardId);

        self::assertEquals($command->cardId(), (string) $card->cardId());
        self::assertEquals($command->title(), $card->title());
        self::assertEquals($command->summary(), $card->summary());
    }

    /**
     * @test
     */
    public function it_responses_with_info_from_card_modified(): void
    {
        $fixture = CardBuilder::create()
            ->withCardId($this->cardId)
            ->fixtures();
        $command = new ModifyCardContentCommand(
            (string) $fixture['cardId'],
            $fixture['title'],
            $fixture['summary']
        );

        $response = $this->handler->handle($command);

        $card = $this->repo->get($this->cardId);

        self::assertEquals((string) $card->cardId(), $response['card_id']);
        self::assertEquals($card->title(), $response['title']);
        self::assertEquals($card->summary(), $response['summary']);
    }
}
