<?php declare(strict_types=1);

namespace Tests\RankUp\Catalog\Application;

use Fixtures\RankUp\Catalog\CardBuilder;
use PHPUnit\Framework\TestCase;
use RankUp\Catalog\Application\CreateCardCommand;
use RankUp\Catalog\Application\CreateCardHandler;
use RankUp\Catalog\Domain\CardId;
use RankUp\Catalog\Infrastructure\Persistence\InMemoryCardRepository;

/**
 * @testdox Command Create Card
 */
class CreateCardHandlerTest extends TestCase
{
    /**
     * @var InMemoryCardRepository
     */
    protected $cardRepository;
    /**
     * @var CreateCardHandler
     */
    protected $createCardHandler;
    /**
     * @var CreateCardCommand
     */
    protected $command;

    protected function setUp(): void
    {
        parent::setUp();

        $this->cardRepository = new InMemoryCardRepository();
        $this->createCardHandler = new CreateCardHandler($this->cardRepository);

        $fixtures = CardBuilder::create()->fixtures();
        $this->command =
            new CreateCardCommand($fixtures['title'], $fixtures['summary'], $fixtures['story']);
    }

    /**
     * @test
     */
    public function it_creates_a_card(): void
    {
        // Given
        $command = $this->command;
        // When
        $response = $this->createCardHandler->handle($command);
        // Then
        $cardId = CardId::fromString($response['card_id']);
        $card = $this->cardRepository->get($cardId);

        self::assertEquals($cardId, $card->cardId());
        self::assertEquals($command->title(), $card->title());
        self::assertEquals($command->summary(), $card->summary());
        self::assertEquals($command->story(), $card->story());
    }

    /**
     * @test
     */
    public function it_responses_with_info_from_card_created(): void
    {
        // Given
        $command = $this->command;
        // When
        $response = $this->createCardHandler->handle($command);
        // Then
        $card = $this->cardRepository->get(CardId::fromString($response['card_id']));

        self::assertNotEmpty((string) $card->cardId(), $response['card_id']);
        self::assertEquals($card->title(), $response['title']);
        self::assertEquals($card->summary(), $response['summary']);
        self::assertEquals($card->story(), $response['story']);
    }
}
