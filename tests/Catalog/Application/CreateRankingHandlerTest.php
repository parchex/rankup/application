<?php declare(strict_types=1);

namespace Tests\RankUp\Catalog\Application;

use Fixtures\RankUp\Catalog\RankingBuilder;
use PHPUnit\Framework\TestCase;
use RankUp\Catalog\Application\CreateRankingCommand;
use RankUp\Catalog\Application\CreateRankingHandler;
use RankUp\Catalog\Domain\RankingId;
use RankUp\Catalog\Infrastructure\Persistence\InMemoryRankingRepository;

/**
 * @testdox Command Create Ranking
 */
class CreateRankingHandlerTest extends TestCase
{
    /**
     * @test
     */
    public function it_creates_a_ranking_from_command_values(): void
    {
        $repo = new InMemoryRankingRepository();
        $handler = new CreateRankingHandler($repo);

        $fixture = RankingBuilder::create()->fixtures();
        $command = new CreateRankingCommand($fixture['title'], $fixture['description']);

        $response = $handler->handle($command);

        $rankingId = RankingId::fromString($response['ranking_id']);
        $ranking = $repo->get($rankingId);

        self::assertEquals($rankingId, $ranking->rankingid());
        self::assertEquals($command->title(), $ranking->title());
        self::assertEquals($command->description(), $ranking->description());
    }

    /**
     * @test
     */
    public function it_responses_with_info_from_ranking_created(): void
    {
        $repo = new InMemoryRankingRepository();
        $handler = new CreateRankingHandler($repo);

        $fixture = RankingBuilder::create()->fixtures();
        $command = new CreateRankingCommand($fixture['title'], $fixture['description']);

        $response = $handler->handle($command);

        $ranking = $repo->get(RankingId::fromString($response['ranking_id']));

        self::assertEquals((string) $ranking->rankingId(), $response['ranking_id']);
        self::assertEquals($ranking->title(), $response['title']);
        self::assertEquals($ranking->description(), $response['description']);
    }
}
