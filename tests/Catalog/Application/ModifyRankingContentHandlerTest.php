<?php declare(strict_types=1);

namespace Tests\RankUp\Catalog\Application;

use Fixtures\RankUp\Catalog\RankingBuilder;
use Fixtures\RankUp\Catalog\RankingProvider;
use PHPUnit\Framework\TestCase;
use RankUp\Catalog\Application\ModifyRankingContentCommand;
use RankUp\Catalog\Application\ModifyRankingContentHandler;
use RankUp\Catalog\Infrastructure\Persistence\InMemoryRankingRepository;

/**
 * @testdox Modify Ranking Content Command
 */
class ModifyRankingContentHandlerTest extends TestCase
{
    /**
     * @var ModifyRankingContentHandler
     */
    protected $handler;
    /**
     * @var array
     */
    protected $fixture;
    /**
     * @var InMemoryRankingRepository
     */
    protected $repo;

    protected function setUp(): void
    {
        $this->fixture = RankingBuilder::create()->fixtures();

        $this->repo = new InMemoryRankingRepository(
            [
                RankingProvider::ranking(),
                RankingProvider::ranking(),
                RankingBuilder::create()->build(),
                RankingProvider::ranking(),
                RankingProvider::ranking(),
            ]
        );
        $this->handler = new ModifyRankingContentHandler($this->repo);
    }

    /**
     * @test
     */
    public function it_gets_ranking_from_repository_and_modifies_content_from_command()
    {
        $command = new ModifyRankingContentCommand(
            (string) $this->fixture['rankingId'],
            $this->fixture['title'],
            $this->fixture['description']
        );

        $this->handler->handle($command);

        $ranking = $this->repo->get($this->fixture['rankingId']);

        self::assertEquals($command->rankingId(), (string) $ranking->rankingId());
        self::assertEquals($command->title(), $ranking->title());
        self::assertEquals($command->description(), $ranking->description());
    }

    /**
     * @test
     */
    public function it_responses_with_info_from_card_modified(): void
    {
        $command = new ModifyRankingContentCommand(
            (string) $this->fixture['rankingId'],
            $this->fixture['title'],
            $this->fixture['description']
        );

        $response = $this->handler->handle($command);

        $ranking = $this->repo->get($this->fixture['rankingId']);

        self::assertEquals((string) $ranking->rankingid(), $response['ranking_id']);
        self::assertEquals($ranking->title(), $response['title']);
        self::assertEquals($ranking->description(), $response['description']);
    }
}
