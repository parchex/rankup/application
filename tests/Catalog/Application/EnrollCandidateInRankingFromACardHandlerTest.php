<?php declare(strict_types=1);

namespace Tests\RankUp\Catalog\Application;

use Fixtures\RankUp\Catalog\CardProvider;
use Parchex\Common\Func;
use Parchex\Core\Domain\Entity;
use RankUp\Catalog\Application\EnrollCandidateInRankingFromACardCommand;
use RankUp\Catalog\Application\EnrollCandidateInRankingFromACardHandler;
use RankUp\Catalog\Application\Responses\CandidateResponse;
use RankUp\Catalog\Domain\Candidate;
use RankUp\Catalog\Domain\CandidateId;
use RankUp\Catalog\Domain\Card;
use RankUp\Catalog\Domain\Ranking;

/**
 * @testdox Command Enroll a Candidate in Ranking from Card data
 */
class EnrollCandidateInRankingFromACardHandlerTest extends RankingTestCase
{
    /**
     * @test
     */
    public function it_enrolls_in_ranking_a_candidate_created_from_card_data(): void
    {
        // Given
        $ranking = $this->createRankingFixturesFillingWithCandidateFixtures();
        $card = $this->createCardFixture();

        $command = $this->givenACommandForEnrollACandidateInRankingFromACard($ranking, $card);
        $handler = $this->givenTheHandlerOfCommand();
        // When
        $response = $this->whenTheCommandIsHandled($handler, $command);
        // Then
        $candidate = self::searchCandidateInRankingById($ranking, $response['candidate_id']);
        self::assertNotNull($candidate);
        self::assertCandidateIsCreatedFromCard($card, $candidate);
    }

    /**
     * @test
     */
    public function it_responses_candidate_info(): void
    {
        // Given
        $ranking = $this->createRankingFixturesFillingWithCandidateFixtures();
        $card = $this->createCardFixture();

        $command = $this->givenACommandForEnrollACandidateInRankingFromACard($ranking, $card);
        $handler = $this->givenTheHandlerOfCommand();
        // When
        $response = $this->whenTheCommandIsHandled($handler, $command);
        // Then
        self::assertNotEmpty($response['candidate_id']);
        self::assertEquals((string) $card->cardId(), $response['card_id']);
        self::assertEquals($card->title(), $response['title']);
        self::assertEquals($card->summary(), $response['presentation']);
    }

    /**
     * @return Card
     */
    protected function createCardFixture(): Card
    {
        $card = CardProvider::card();
        $this->cardRepo->storage()->set($card);

        return $card;
    }

    protected function givenACommandForEnrollACandidateInRankingFromACard(
        Ranking $ranking,
        Card $card
    ): EnrollCandidateInRankingFromACardCommand {
        return new EnrollCandidateInRankingFromACardCommand(
            (string) $ranking->rankingId(),
            (string) $card->cardId()
        );
    }

    protected function givenTheHandlerOfCommand(): EnrollCandidateInRankingFromACardHandler
    {
        return new EnrollCandidateInRankingFromACardHandler(
            $this->rankingRepo,
            $this->cardRepo
        );
    }

    protected function whenTheCommandIsHandled(
        EnrollCandidateInRankingFromACardHandler $handler,
        EnrollCandidateInRankingFromACardCommand $command
    ): CandidateResponse {
        return $handler->handle($command);
    }

    protected static function searchCandidateInRankingById(
        Ranking $ranking,
        string $candidateId
    ): ?Candidate {
        $candidateId = CandidateId::fromString($candidateId);
        /** @var Candidate $candidate */
        $candidates = Func::filter(
            function (Entity $entity) use ($candidateId) {
                return $entity->identifier()->equals($candidateId);
            },
            $ranking->candidates()
        );

        return $candidates[0] ?? null;
    }
}
