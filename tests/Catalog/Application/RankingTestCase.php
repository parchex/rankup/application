<?php declare(strict_types=1);

namespace Tests\RankUp\Catalog\Application;

use Fixtures\RankUp\Catalog\CandidateBuilder;
use Fixtures\RankUp\Catalog\CandidateProvider;
use Fixtures\RankUp\Catalog\CardBuilder;
use Fixtures\RankUp\Catalog\RankingBuilder;
use Parchex\Common\Func;
use PHPUnit\Framework\TestCase;
use RankUp\Catalog\Domain\Candidate;
use RankUp\Catalog\Domain\Card;
use RankUp\Catalog\Domain\Ranking;
use RankUp\Catalog\Infrastructure\Persistence\InMemoryCandidateRepository;
use RankUp\Catalog\Infrastructure\Persistence\InMemoryCardRepository;
use RankUp\Catalog\Infrastructure\Persistence\InMemoryRankingRepository;
use Tests\Parchex\PHPUnit\TestCases\CollectionTestCase;

abstract class RankingTestCase extends TestCase
{
    use CollectionTestCase;

    /**
     * @var InMemoryRankingRepository
     */
    protected $rankingRepo;
    /**
     * @var InMemoryCardRepository
     */
    protected $cardRepo;
    /**
     * @var InMemoryCandidateRepository
     */
    protected $candidateRepo;

    protected function setUp(): void
    {
        $this->rankingRepo = new InMemoryRankingRepository();
        $this->cardRepo = new InMemoryCardRepository();
        $this->candidateRepo = new InMemoryCandidateRepository();

        $this->createRankingFixturesFillingWithCandidateFixtures();
        $this->createRankingFixturesFillingWithCandidateFixtures();
        $this->createRankingFixturesFillingWithCandidateFixtures();
    }

    protected static function assertCardIsCreatedForCandidate(
        Candidate $candidate,
        Card $card
    ): void {
        self::assertEquals($candidate->cardId(), $card->cardId());
        self::assertEquals($candidate->title(), $card->title());
        self::assertEquals($candidate->presentation(), $card->summary());
    }

    protected static function assertCandidateIsEnrolledInRanking(
        Ranking $ranking,
        Candidate $candidate
    ): void {
        self::assertCollectionContainsItem($candidate, $ranking->candidates());
    }

    protected static function assertCandidateIsCreatedFromCard(
        Card $card,
        Candidate $candidate
    ): void {
        self::assertEquals($card->cardId(), $candidate->cardId());
        self::assertEquals($card->title(), $candidate->title());
        self::assertEquals($card->summary(), $candidate->presentation());
    }

    protected function createRankingFixturesFillingWithCandidateFixtures(): Ranking
    {
        $candidates = $this->createCandidates();

        $this->createdCards($candidates);

        return $this->createRanking($candidates);
    }

    protected function createCandidates(): array
    {
        $candidates = CandidateProvider::entitiesBetween(CandidateBuilder::class, 5, 10);
        $this->candidateRepo = new InMemoryCandidateRepository($candidates);
        Func::map([$this->candidateRepo->storage(), 'set'], $candidates);

        return $candidates;
    }

    protected function createdCards(array $candidates): array
    {
        $cards = Func::map(
            function (Candidate $candidate) {
                return CardBuilder::create()
                    ->withCardId($candidate->cardId())
                    ->withTitle($candidate->title())
                    ->build();
            },
            $candidates
        );
        Func::map([$this->cardRepo->storage(), 'set'], $cards);

        return $cards;
    }

    protected function createRanking(array $candidates): Ranking
    {
        $ranking = RankingBuilder::create()
            ->withCandidates($candidates)
            ->withMaximumCandidates(count($candidates) + 10)
            ->build();
        $this->rankingRepo->storage()->set($ranking);

        return $ranking;
    }
}
