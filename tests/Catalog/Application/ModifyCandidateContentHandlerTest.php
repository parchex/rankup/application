<?php declare(strict_types=1);

namespace Tests\RankUp\Catalog\Application;

use Fixtures\RankUp\Catalog\CandidateBuilder;
use Fixtures\RankUp\Catalog\CandidateProvider;
use PHPUnit\Framework\TestCase;
use RankUp\Catalog\Application\ModifyCandidateContentCommand;
use RankUp\Catalog\Application\ModifyCandidateContentHandler;
use RankUp\Catalog\Infrastructure\Persistence\InMemoryCandidateRepository;

/**
 * @testdox Modify CandidateContent Command
 */
class ModifyCandidateContentHandlerTest extends TestCase
{
    /**
     * @var InMemoryCandidateRepository
     */
    protected $repo;
    /**
     * @var ModifyCandidateContentHandler
     */
    protected $handler;
    /**
     * @var \RankUp\Catalog\Domain\CandidateId
     */
    protected $candidateId;

    protected function setUp(): void
    {
        $candidate = CandidateProvider::candidate();
        $this->candidateId = $candidate->candidateId();

        $this->repo = new InMemoryCandidateRepository(
            [
                CandidateProvider::candidate(),
                CandidateProvider::candidate(),
                $candidate,
                CandidateProvider::candidate(),
                CandidateProvider::candidate(),
            ]
        );
        $this->handler = new ModifyCandidateContentHandler($this->repo);
    }

    /**
     * @test
     */
    public function it_gets_candidate_from_repository_and_modifies_content_from_command(): void
    {
        $fixture = CandidateBuilder::create()
            ->withCandidateId($this->candidateId)
            ->fixtures();
        $command = new ModifyCandidateContentCommand(
            (string) $fixture['candidateId'],
            $fixture['title'],
            $fixture['presentation']
        );

        $this->handler->handle($command);

        $candidate = $this->repo->get($this->candidateId);
        self::assertEquals($command->candidateId(), (string) $candidate->candidateId());
        self::assertEquals($command->title(), $candidate->title());
        self::assertEquals($command->presentation(), $candidate->presentation());
    }

    /**
     * @test
     */
    public function it_responses_with_info_from_candidate_modified(): void
    {
        $fixture = CandidateBuilder::create()
            ->withCandidateId($this->candidateId)
            ->fixtures();
        $command = new ModifyCandidateContentCommand(
            (string) $fixture['candidateId'],
            $fixture['title'],
            $fixture['presentation']
        );

        $response = $this->handler->handle($command);

        $candidate = $this->repo->get($this->candidateId);
        self::assertEquals((string) $candidate->candidateId(), $response['candidate_id']);
        self::assertEquals($candidate->title(), $response['title']);
        self::assertEquals($candidate->presentation(), $response['presentation']);
    }
}
